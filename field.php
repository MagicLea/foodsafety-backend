<?php
  include 'function.php';
  class FieldObj {
      var $field_name = '';
      var $field_loc_lat = 0;
      var $field_loc_lng = 0;
      var $field_url = '';
  }
?>
<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Info windows</title>
  </head>
  <body>
    <?php
      function process(){
        global $ansObj;
        $ansObj = new FieldObj();
        if(!isset($_GET['field_id'])){
          echo 'invalid parameter<br>';
          return;
        }
        try {
          $conn = connect_db();
          $fetch = $conn->prepare('SELECT field_name, field_loc_lat, field_loc_lng, field_url FROM fields WHERE field_id = :field_id');
          $fetch->bindParam(':field_id', $_GET['field_id'], PDO::PARAM_STR);
          $fetch->execute();
          $row = $fetch->fetch(PDO::FETCH_ASSOC);
          if( !$row )
          {
            echo 'The field_id is invalid.<br>';
            return;
          }
          else {            
            $ansObj->field_name = $row['field_name'];
            $ansObj->field_loc_lat = (double)$row['field_loc_lat'];
            $ansObj->field_loc_lng = (double)$row['field_loc_lng'];
            $ansObj->field_url = $row['field_url'];
          }
        }
        catch(PDOException $e) {
            echo $e->getMessage();
        }
        return;
      }

      process();
      $conn = null;
    ?>
    <div>
      <h1><?php echo $ansObj->field_name;?></h1>
      <div>
        <p><img src="<?php echo $ansObj->field_url;?>" height="300"></p>
        <p>經度：<?php echo $ansObj->field_loc_lng;?><br>緯度：<?php echo $ansObj->field_loc_lat;?></p>
      </div>
    </div>
  </body>
</html>