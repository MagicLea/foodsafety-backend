<?php
    session_start();
    include 'function.php';
    header('Content-Type: application/json; charset=utf-8');
	
    class ResponseObj {
        var $error = 0;
		var $qrcodes = null;
        var $msg;
    }

    $ans = new ResponseObj();
    
    if (!isset($_SESSION['acc_id'])) {
        $ans->error = 1;
        $ans->msg = 'no login';
        echo json_encode($ans);
        return;
    }
    
    if($_SESSION['acc_type'] != 'ADMIN'){
        $ans->error = 2;
        $ans->msg = 'not a admin';
        echo json_encode($ans);
        return;
    }

    if(!isset($_GET['num']) || !isset($_GET['target_id'])){
        $ans->error = 3;
        $ans->msg = 'invalid parameter';
        echo json_encode($ans);
        return;
    }

    $count = 0;
    try {
        $conn = connect_db();
        #check whether target_id is valid
        $fetch = $conn->prepare('SELECT acc_type FROM accounts WHERE acc_id = :target_id');
        $fetch->bindParam(':target_id', $_GET['target_id'], PDO::PARAM_STR);
        $fetch->execute();
        $row = $fetch->fetch(PDO::FETCH_ASSOC);
        if( !$row )
        {
            $ans->error = 3;
            $ans->msg = "The target_id is invalid." ;
            echo json_encode($ans);
            return;
        }
		#insert
		$conn->beginTransaction();
        $stmt = $conn->prepare('INSERT INTO authorized_code (acc_id,random_code) VALUES (:acc_id, :random_code)');
        $stmt->bindParam(':acc_id', $_GET['target_id'], PDO::PARAM_STR);
        $MAX = (int)pow(16,6)-1;
		$insert_id = array();

        while($count < $_GET['num']){
            $random_code = mt_rand(0,$MAX);
            $stmt->bindValue(':random_code', sprintf('%6X',$random_code), PDO::PARAM_STR);
            $stmt->execute();
			$insert_id[$count] = $conn->lastInsertId();
            $count++;
        }
        $conn->commit();
		#display generated authCode
		$fetch = $conn->prepare('SELECT auth_id, random_code FROM authorized_code WHERE auth_id IN ('.implode(',',$insert_id).')');
        $fetch->execute();
        $row = $fetch->fetch(PDO::FETCH_ASSOC);
		$qrcode = [];
		do {
			$qrcode = $row['auth_id'].'-'.$row['random_code'];
			$ans->qrcodes[] = $qrcode;
		}while($row = $fetch->fetch(PDO::FETCH_ASSOC));
    }
    catch(PDOException $e) {
        $conn->rollback();
        $ans->error = 4;
        $ans->msg = getMessage();
    }
	echo json_encode($ans);
    $conn = null;
?>