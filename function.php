<?php

    function connect_db(){
        try
        {
            $hostname = 'localhost';
            /*** mysql username ***/
            $username = 'foodadmin';
            /*** mysql password ***/
            $password = 'foodfood';            
            $options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8mb4'); 
            $connection = new PDO("mysql:host=$hostname;dbname=food_safety", $username, $password, $options);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch (PDOException $e)
        {
            // Proccess error
             echo 'Cannot connect to database: ' . $e->getMessage();
            return null;
        }
        return $connection;
    }
?>
