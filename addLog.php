<?php
    session_start();
    include 'function.php';
    header('Content-Type: application/json; charset=utf-8');

    class ResponseObj {
        var $error = 0;
        var $msg;
    }

    $ans = new ResponseObj();
    
    if (!isset($_SESSION['acc_id'])) {
        $ans->error = 1;
        $ans->msg = 'no login';
        echo json_encode($ans);
        return;
    }
    
    if(!isset($_GET['field_id']) || !isset($_GET['qr_code'])){
        $ans->error = 2;
        $ans->msg = 'invalid parameter';
        echo json_encode($ans);
        return;
    }

    try {
        $conn = connect_db();
        #check whether field_id is valid
        $fetch = $conn->prepare('SELECT field_id FROM fields WHERE acc_id = :acc_id AND field_id = :field_id');
        $fetch->bindParam(':acc_id', $_SESSION['acc_id'], PDO::PARAM_STR);
        $fetch->bindParam(':field_id', $_GET['field_id'], PDO::PARAM_STR);
        $fetch->execute();
        $row = $fetch->fetch(PDO::FETCH_ASSOC);
        if( !$row )
        {
            $ans->error = 3;
            $ans->msg = "The field_id is unauthorized." ;
            echo json_encode($ans);
            return;
        }
        #check whether qrcode is valid
        $tmp = explode("-", $_GET['qr_code']);
        if(count($tmp)!=2)
		{
			$ans->error = 4;
            $ans->msg = "The QR code is invalid." ;
            echo json_encode($ans);
            return;
		}
		list($auth_id,$random_code) = $tmp;
		$fetch = $conn->prepare('SELECT auth_id FROM authorized_code WHERE auth_id = :auth_id AND random_code = :random_code');
        $fetch->bindParam(':auth_id', $auth_id, PDO::PARAM_STR);
        $fetch->bindParam(':random_code', $random_code, PDO::PARAM_STR);
        $fetch->execute();
        $row = $fetch->fetch(PDO::FETCH_ASSOC);
        if( !$row )
        {
            $ans->error = 4;
            $ans->msg = "The QR code is invalid." ;
            echo json_encode($ans);
            return;
        }

        #insert a log
        $message = '';
        switch ($_SESSION['acc_type']) {
            case "FARMER":
                $message = 'Be harvested.(採收)';
                break;
            case "MARKET":
                $message = 'on-shelf (上架)';
                break;
			case "TRANS":
                $message = 'Arrived (已抵達)';
                break;
        }
        $stmt = $conn->prepare('INSERT INTO logs (recoder_id, auth_id, field_id, message) VALUES (:recoder_id, :auth_id, :field_id, :message)');
        $stmt->bindParam(':recoder_id', $_SESSION['acc_id'], PDO::PARAM_STR);
        $stmt->bindParam(':auth_id', $auth_id, PDO::PARAM_STR);
        $stmt->bindParam(':field_id', $_GET['field_id'], PDO::PARAM_STR);
        $stmt->bindParam(':message', $message, PDO::PARAM_STR);
        $stmt->execute();
    }
    catch(PDOException $e) {
        $ans->error = 4;
        $ans->msg = $e->getMessage();
    }
    $conn = null;
    echo json_encode($ans);
?>