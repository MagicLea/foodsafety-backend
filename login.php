<?php
    session_start();
    include 'function.php';
    header('Content-Type: application/json; charset=utf-8');

    class ResponseObj {
        var $error = 0;
        var $msg;
        var $sessionID;
		var $accType;
    }

    $ans = new ResponseObj();

    if(!isset($_GET['acc']) || !isset($_GET['pwd'])){
        $ans->error = 1;
        $ans->msg = 'invalid parameter';
        echo json_encode($ans);
        return;
    }

    try {
        $conn = connect_db();
        $fetch = $conn->prepare('SELECT acc_id, acc_type FROM accounts WHERE acc_id = :acc_id AND acc_pwd = SHA1(:acc_pwd)');
        $fetch->bindParam(':acc_id', $_GET['acc'], PDO::PARAM_STR);
        $fetch->bindParam(':acc_pwd', $_GET['pwd'], PDO::PARAM_STR);
        $fetch->execute();
        $row = $fetch->fetch(PDO::FETCH_ASSOC);
        
        if( !$row )
        {
            $ans->error = 2;
            $ans->msg = "wrong account information" ;
            session_unset();
        }
        else
        {
            $_SESSION['acc_id'] = $row['acc_id'];
            $_SESSION['acc_type'] = $row['acc_type'];
            $ans->msg = "";
			$ans->accType = $row['acc_type'];
            $ans->sessionID = session_id();
        }
        echo json_encode($ans);
    }
    catch(PDOException $e)
    {
        $ans->error = 3;
        $ans->msg = $e->getMessage() ;
    }
    $conn = null;
?>