<?php
    include 'function.php';
    class LogObj {
        var $field_id;
        var $fieldName;
        var $loc;
        var $logMessage;
        var $timestamp;
    }
    class LocObj {
        var $lng;
        var $lat;
    }
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <style type="text/css">
      html, body { height: 100%; margin: 0; padding: 0; }
      #map { height: 87%; }
    </style>
  </head>
  <body>
    <div id="map"></div>
    <?php
      function process(){
        if(!isset($_GET['qr_code'])){
          echo 'invalid parameter<br>';
          return;
        }
        try {
          $conn = connect_db();
          #check whether qrcode is valid
          list($auth_id,$random_code) = explode("-", $_GET['qr_code']);
          $fetch = $conn->prepare('SELECT auth_id FROM authorized_code WHERE auth_id = :auth_id AND random_code = :random_code');
          $fetch->bindParam(':auth_id', $auth_id, PDO::PARAM_STR);
          $fetch->bindParam(':random_code', $random_code, PDO::PARAM_STR);
          $fetch->execute();
          $row = $fetch->fetch(PDO::FETCH_ASSOC);
          if( !$row )
          {
              echo 'The QR code is invalid.<br>';
              return;
          }
          #get logs
          $fetch = $conn->prepare('SELECT logs.field_id, field_name, message, timestamp, field_loc_lat, field_loc_lng FROM logs, fields WHERE auth_id = :auth_id AND fields.field_id = logs.field_id ORDER BY timestamp ASC');
          $fetch->bindParam(':auth_id', $auth_id, PDO::PARAM_STR);
          $fetch->execute();
          $row = $fetch->fetch(PDO::FETCH_ASSOC);
          if( !$row )
          {
            echo 'No records are found.<br>';
            return;
          }
          else {
			global $logs;
			global $coordinates;
            $logs = [];
			$coordinates = [];
            do{
              $log = new LogObj();
              $log->field_id = (int)$row['field_id'];
              $log->fieldName = $row['field_name'];
              $loc = new LocObj();
              $loc->lng = (double)$row['field_loc_lng'];
              $loc->lat = (double)$row['field_loc_lat'];
              $log->loc = $loc;
              $log->logMessage = $row['message'];
              $log->timestamp = $row['timestamp'];
			  echo '<b>'.$log->fieldName.'</b> - '.$log->logMessage.' ('.$log->timestamp.')<br>';
              array_push($logs,$log);
			  array_push($coordinates,$loc);
            }while($row = $fetch->fetch(PDO::FETCH_ASSOC));
          }
        }
        catch(PDOException $e) {
            echo $e->getMessage();
        }
        return;
      }

      process();
      $conn = null;
    ?>	
    <script type="text/javascript">

var map;
function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: 23.975186, lng: 121.073292},
    zoom: 8
  });
  

  var infowindow = new google.maps.InfoWindow();
  <?php
	echo 'var logs = '.json_encode($logs).';';
	echo 'var coordinates = '.json_encode($coordinates).';';
  ?>
  var polyPath = new google.maps.Polyline({
	  path: coordinates,
	  geodesic: true,
	  strokeColor: '#000000',
	  strokeOpacity: 0.8,
	  strokeWeight: 2
  })
  polyPath.setMap(map);
  var markers = [];
  for(var i=0; i<logs.length;i++){
    markers[i] = new google.maps.Marker({
      position: logs[i].loc,
      map: map,
      title: logs[i].fieldName,
	  info: getContentText(logs[i].fieldName,logs[i].field_id,logs[i].logMessage,logs[i].timestamp)
    });
    markers[i].addListener('click', function() {
      infowindow.open(map, this);
      infowindow.setContent(this.info);
    });
  }
}
function getContentText(fieldName, fieldID, logMessage, timestamp){
  return '<div id="content">'+
      '<div id="siteNotice">'+
      '</div>'+
      '<h1 id="firstHeading" class="firstHeading">'+ fieldName +'</h1>'+
      '<div id="bodyContent">'+
      '<p>'+ logMessage +' ('+ timestamp +')</p>'+
      '<p>Related Link: '+ fieldName +
      ', <a href="field.php?field_id='+ fieldID +'">'+'http://<?php echo $_SERVER['HTTP_HOST']?>/'
	  +'field.php?field_id='+ fieldID +'</a> .</p>'+
      '</div>'+
      '</div>';
}

    </script>
    <script async defer
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDvB83fc6CF4liT2jCZTKinuqkqxPFvZGo&callback=initMap">
    </script>
  </body>
</html>